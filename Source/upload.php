<?php
//$upload_dir = "/tmp/";
require("ConnectionClass.php");
require("Node_Decoder.php");
require_once("Node.php");

if ($_FILES["fileToUpload"]["error"] == 0) {
    $upload = true;
    $doc = new DOMDocument();
    error_reporting(E_ERROR | E_PARSE);
    if ($doc->load($_FILES["fileToUpload"]["tmp_name"])) {
        $db = new ConnectionClass();
        $hash = hash("sha256", $doc->saveXML());
        if (!$db->check_if_stored($hash)) {
            $upload = false;
            echo "File like this is already stored. \n";
        }
    } else {
        $upload = false;
        echo "Selected file is not a XML.\n";
    }
    error_reporting(E_ALL);

    if ($upload) {
        $target_dir = "repository/";
        $file_name = basename($_FILES["fileToUpload"]["name"]);
        $target_file = $target_dir . $file_name;
        if (file_exists($target_file)) {
            $file_name = explode('.', $file_name);
            $extension = array_pop($file_name);
            $file_name = implode('.', $file_name);
            $file_name = $file_name . date("-d.m.y-H.i.s");
//                $file_name = $file_name . "a";
            $file_name = $file_name . '.' . $extension;
            echo "<p>
File with this name already exists. At the names end date has been added.
</p>";
        }
        $target_file = $target_dir . $file_name;
        move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file);
        error_reporting(E_ERROR | E_PARSE);
        $nodes = Node_Decoder::domToArray($doc);
        error_reporting(E_ALL);
        if (isset($db) & isset($hash)) {
            $db->insert_file($file_name, $target_dir, $hash);
            $db->insert_nodes($file_name, $target_dir, $nodes);
            $db->disconnect();
        }
    }

} elseif ($_FILES["fileToUpload"]["error"] == 1) {
    $upload = false;
    echo "Selected file is too large";
}

if ($_POST["html"] == true) {
    echo "
    <div>
    <form action='upload_form.php'>
        <input type='submit' value='Back'/>
    </form>
</div>
";
}