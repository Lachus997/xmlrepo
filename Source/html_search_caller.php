<?php
require_once("ConnectionClass.php");
require_once("Searching_Class.php");
if ($_POST["node_id"] . $_POST["node_name"] == "") {
    echo "You have to choose value.";
} else {
    $search = new SearchingFile(new HTML_presenter(), "XMLRepoFileHTML");
    $search->search($_POST["node_id"], $_POST["node_name"]);
}
?>
<div>
    <form action='search_form.php'>
        <input type='submit' value='Back'/>
    </form>
</div>
