<?php
require_once("Printer.php");
require_once("XMLRepoFile.php");

class XMLRepoFileHTML extends XMLRepoFile implements Printer
{
    public function printAll()
    {
        echo "<tr>";
        self::printColumnVal($this->id);
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
        echo "</tr>";
    }

    public function printWithoutId()
    {
        echo "<tr>";
        self::printColumnVal($this->file_name);
        self::printColumnVal($this->file_path);
        echo "</tr>";
    }

    public static function printColumnVal($val)
    {
        echo "<td>$val</td>";
    }
}