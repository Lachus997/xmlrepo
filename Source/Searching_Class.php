<?php
require_once("XMLRepoFileCLI.php");
require_once("XMLRepoFileHTML.php");
require_once("ConnectionClass.php");
require_once("HTML_presenter.php");
require_once("Cli_presenter.php");

class SearchingFile
{
    private Data_presenter $data_presenter;
    private string $class_name;

    /**
     * SearchingFile constructor.
     * @param Data_presenter $data_presenter
     * @param string $class_name
     */
    public function __construct(Data_presenter $data_presenter, string $class_name)
    {
        $this->data_presenter = $data_presenter;
        $this->class_name = $class_name;
    }

    public function search($id, $name)
    {
        if ($name . $id == "") {
            echo "You have to choose value. \n";
        } else {
            $db = new ConnectionClass();

            $resoult = $db->search_file($id, $name, $this->class_name);
            $db->disconnect();

            $this->data_presenter->print_header();
            foreach ($resoult as $item) {
                $item->printWithoutId();
            }
            $this->data_presenter->print_bottom();
        }
    }
}