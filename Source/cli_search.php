#!G:\Lachus\JDKs\PHP\php-8.0.3\php.exe
<?php
require_once("Searching_Class.php");
echo "Searching for files";

if ($args = getopt("i:n:")) {
    $id = "";
    $name = "";
    if (array_key_exists("i", $args)) {
        $id = $args['i'];
    }
    if (array_key_exists("n", $args)) {
        $name = $args['n'];
    }

    if ($id . $name != "") {
        $search = new SearchingFile(new Cli_presenter(), "XMLRepoFileCLI");
        $search->search($id, $name);
    } else {
        echo "Values of neither id or name is not set";
    }
} else {
    echo "To search file you need to pass either name (-n), id (-i) or both.";
}
