<?php

interface Printer
{
    public function printAll();

    public function printWithoutId();
}
